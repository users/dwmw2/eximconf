CREATE TABLE resenders (
	host		TEXT PRIMARY KEY
);

CREATE TABLE greylist (
	id		TEXT PRIMARY KEY,
	expire		INTEGER,
	host		TEXT
);

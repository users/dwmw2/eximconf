#!/usr/bin/perl -w

use strict;
use DBI qw(:sql_types);

my $confdir = "/etc/exim";
my $dbfn = "$confdir/greylist.db";

my $dbh = DBI->connect("dbi:SQLite:dbname=$dbfn", "", "",
	{ AutoCommit => 1 });
$dbh->{unicode} = 1;
die "connect($dbfn) failed: " . DBI->errstr . "\n"
	unless $dbh;

my $expire_time = time - (60 * 60 * 24 * 7);
$dbh->do('DELETE FROM greylist WHERE expire < $expire_time') or
	die "delete($expire_time) failed: " . DBI->errstr . "\n";
$dbh->disconnect;
exit(0);

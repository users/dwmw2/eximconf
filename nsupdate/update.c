/* $Id: update.c,v 1.1 2005/09/10 10:08:28 dwmw2 Exp $ */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

void nsupdate(char *fmt, ...)
{
	va_list args;
	int sockfds[2];
	pid_t childpid;
	FILE *f;

	if (socketpair(AF_UNIX, SOCK_STREAM, 0, sockfds)) {
		perror("socketpair");
		exit(1);
	}

	childpid = fork();
	if (!childpid) {
		close(sockfds[0]);
		dup2(sockfds[1], 0);
		close(1);
		open("/dev/null", O_WRONLY); // will be stdout 
		execl("/usr/bin/nsupdate", "nsupdate", "-k", KEYFILE, NULL);
		exit(1);
	} else if (childpid == -1) {
		close(sockfds[0]);
		close(sockfds[1]);
		perror("fork");
		exit(1);
	}
	close(sockfds[1]);
	f = fdopen(sockfds[0], "w");
	if (!f) {
		perror("fdopen");
		exit(1);
	}
	fprintf(f, "server "NAMESERVER"\n");
	fprintf(f, "zone "LOOKUPDOMAIN".\n");
	va_start(args, fmt);
	vfprintf(f, fmt, args);
	va_end(args);
	fprintf(f, "send\n");
	fclose(f);
	close(sockfds[1]);
	waitpid(childpid, NULL, 0);
}

void testaddr(char *addr)
{
	execl("/usr/sbin/exim", "exim", "-bt", addr, NULL);
}

int main(int argc, char **argv)
{
	uid_t me;
	struct passwd *pw;

	me = getuid();
	pw = getpwuid(me);
	if (!pw) {
		perror ("getpwuid");
		exit(1);
	}
	setuid(geteuid());
	if (argc < 2 || (argc < 4 && !strcasecmp(argv[1], "show"))) {
		testaddr(argc==3?argv[2]:pw->pw_name);
	} else if (argc == 2 && !strcasecmp(argv[1], "unalias")) {
		printf("Removing alias for %s@" MAILDOMAIN "\n", pw->pw_name);
		nsupdate("update delete %s." LOOKUPDOMAIN".\n", pw->pw_name);
		sleep(PROPAGATION_TIME);
		testaddr(pw->pw_name);
	} else if (!me && argc == 3 && !strcasecmp(argv[1], "unalias")) {
		printf("Removing alias for %s@" MAILDOMAIN "\n", argv[2]);
		nsupdate("update delete %s." LOOKUPDOMAIN".\n", argv[2]);
		sleep(PROPAGATION_TIME);
		testaddr(argv[2]);
	} else if (argc == 3 && !strcasecmp(argv[1], "alias")) {
		printf("Setting alias for %s@" MAILDOMAIN " to %s\n", pw->pw_name, argv[2]);
		nsupdate("update delete %s."LOOKUPDOMAIN".\nupdate add %s."LOOKUPDOMAIN". %d TXT %s\n",
			 pw->pw_name, pw->pw_name, TTL, argv[2]);
		sleep(PROPAGATION_TIME);
		testaddr(pw->pw_name);
	} else if (!me && argc == 4 && !strcasecmp(argv[1], "alias")) {
		printf("Setting alias for %s@" MAILDOMAIN " to %s\n", argv[2], argv[3]);
		nsupdate("update delete %s."LOOKUPDOMAIN".\nupdate add %s."LOOKUPDOMAIN". %d TXT %s\n",
			 argv[2], argv[2], TTL, argv[3]);
		sleep(PROPAGATION_TIME);
		testaddr(argv[2]);
	} else {
		if (me) {
			fprintf(stderr, "Usage: "PROGNAME" alias <address>\n");
			fprintf(stderr, "  or   "PROGNAME" unalias\n");
		} else {
			fprintf(stderr, "Usage: "PROGNAME" alias <user> <address>\n");
			fprintf(stderr, "  or   "PROGNAME" unalias <user>\n");
		}
		fprintf(stderr, "  or   "PROGNAME" show [<user>]\n");
	}


}

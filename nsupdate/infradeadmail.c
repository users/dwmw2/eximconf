/* $Id: infradeadmail.c,v 1.1 2005/09/10 10:08:28 dwmw2 Exp $ */

#define MAILDOMAIN "infradead.org"
#define LOOKUPDOMAIN "infradead.org.mail"
#define NAMESERVER "casper.infradead.org"
#define KEYFILE "/var/named/keys/Kinfradead.org.mail.+157+51373.key"
#define PROGNAME "infradeadmail"
#define TTL 604800
#define PROPAGATION_TIME 1

#include "update.c"

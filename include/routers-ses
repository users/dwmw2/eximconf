# $Id: routers-ses,v 1.5 2008/12/10 14:39:24 dwmw2 Exp $

  # Verify, and extract return address from, an SRS-address.
  # Don't allow non-bounces, except from postmaster@* since some people use
  # that for sender-verification callbacks.
rpr_bounce:
  caseful_local_part
  driver = redirect
  domains = +rpr_domains
  allow_fail
  data = ${if !match {$local_part}{\N^[bBsS][aArR][tTsS][vV0]\+([^+]+)\+([0-9]+)\+([^+]+)\+(.*)\N} \
		{:fail: Invalid SRS bounce \
.ifdef SRS_DEBUG
			(malformed)\
.endif
		} \
 	{${if and {{!eq {$1}{${length_SRS_HASH_LENGTH:${hmac{md5}{SRS_SECRET}{${lc:$2+$3+$4@$domain}}}}}} \
.ifdef SRS_OLD_SECRET
		  {!eq {$1}{${length_SRS_HASH_LENGTH:${hmac{md5}{SRS_OLD_SECRET}{${lc:$2+$3+$4@$domain}}}}}} \
.endif
		  } \
		{:fail: Invalid SRS bounce \
.ifdef SRS_DEBUG
			(HMAC should be ${length_SRS_HASH_LENGTH:${hmac{md5}{SRS_SECRET}{${lc:$2+$3+$4@$domain}}}} not $1)\
.endif
		} \
	{${if <{$2}{${eval:$tod_epoch/86400-12288-SRS_DSN_TIMEOUT}} \
		{:fail: Invalid SRS bounce \
.ifdef SRS_DEBUG
			(expired ${eval:$tod_epoch/86400-12288-SRS_DSN_TIMEOUT-$2} days ago)\
.endif
		} \
	{${if >{$2}{${eval:($tod_epoch+43200)/86400-12288}} \
		{:fail: Invalid SRS bounce \
.ifdef SRS_DEBUG
			(timestamp in future by more than 43200 seconds)\
.endif
		} \
	{${if and { {!eq {$sender_address}{}} \
	            {!eqi {$sender_address_local_part}{postmaster}}\
	            {!eqi {$sender_address_local_part}{double-bounce}}\
	            {!eqi {$sender_address}{ietf-action@ietf.org}}\
	            {!eqi {$sender_address_domain}{lists.oasis-open.org}}\
	            {!eqi {$sender_address_domain}{ietfa.amsl.com}}\
	            {!eqi {$sender_address_domain}{followup.ticket.aaisp.net.uk}}\
		  } \
		{:fail: Invalid SRS bounce \
.ifdef SRS_DEBUG
			(Not DSN: $sender_address instead)\
.endif
		}\
# Wheee. At last the actual rewrite part...
	{${quote_local_part:$4}@$3}\
	}}}}}}}}}
  headers_add = X-SRS-Return: DSN routed via $primary_hostname. See SRS_URL

# Rewrite reverse-path so that forwarding to known SPF-afflicted
# servers doesn't break. We generate a limited-lifetime hash cookie,
# from which we can later recreate the original sender address. We
# include the hostname and more precise timestamp in the domain of the
# generated address, so that we can track down the offending message
# in the log if it _does_ offend us.
	
.ifdef SRS_DOMAIN
rpr_outgoing_goto:
  caseful_local_part
  driver = redirect
  # Don't rewrite unless the recipient is in a domain we _know_ to be broken
  # but for local reasons have decided we need to work around.
  # The text file listing broken recipient domains should look something like:
  #    gmx.net: all
  #    gmx.de: all
  #    aol.com: spf
  # This lookup will leave the result of the lookup in $domain_data.
  domains = lsearch;CONFDIR/conf/spf-afflicted-domains
  # Don't rewrite if it's a bounce, or from one of our own addresses.
  senders = ! : ! *@+local_domains : ! *@+virtual_domains
  # We expect any or all of 'all', 'spf', 'cid' or 'self' in $domain_data from the textfile lookup
  # If the reason for the breakage is listed as 'spf', then don't rewrite
  # unless the sender's domain actually advertises SPF records.
  # Likewise for 'cid' and Caller-ID records.
  # If it's listed as 'self' then rewrite only if we're sending it
  # mail claiming to be from itself or one of its subdomains.
  # If it's 'all' then just rewrite everything.
  condition = ${if or { \
			{eq{$domain_data}{all}} \
			{and {	{match{$domain_data}{spf}} \
				{match {${lookup dnsdb{txt=$sender_address_domain}{$value}{}}}{v=spf1}} \
			     }} \
			{and {	{match{$domain_data}{cid}} \
				{match {${lookup dnsdb{txt=_ep.$sender_address_domain}{$value}{}}}{^<ep}} \
			     }} \
			{and {	{match{$domain_data}{self}} \
				{match {$sender_address_domain}{${rxquote:$domain}\$}} \
			     }} \
			} {1}}
  # We want to rewrite. We just jump to the rpr_rewrite router which is itself unconditional.
  data = ${quote_local_part:$local_part}@$domain
  redirect_router = rpr_rewrite

  # Some addresses are joe-job protected by _always_ using SRS, and never actually
  # sending mail from that address. That way, we can always reject bounces to these
  # addresses, and prevent joe-jobs from being received by anyone who actually bothers
  # to do sender verification callouts.
rpr_always_else:
  caseful_local_part
  driver = redirect
  senders = !@@lsearch;CLUSTER/always-srs-senders
  data = ${quote_local_part:$local_part}@$domain
  redirect_router = lookuphost


  # This is now unconditional. Either rpr_outgoing_goto jumped here
  # because it's a mail we're forwarding to a known broken server, or
  # rpr_always_else _didn't_ jump over us because it's from a sender
  # listed in always-srs-senders. This would be a lot nicer if we had
  # the matchdomain/matchaddress items from the WishList and could cut
  # it down to just this router with appropriate conditions instead of
  # the 'goto' mess above.
rpr_rewrite:
  caseful_local_part
  headers_add = "X-SRS-Rewrite: SMTP reverse-path rewritten from <$sender_address> by $primary_hostname. See SRS_URL"
  # Encode sender address, hash and timestamp according to http://www.anarres.org/projects/srs/
  # We try to keep the generated localpart small. We add our own tracking info to the domain part.
  address_data = ${eval:($tod_epoch/86400)-12288}+\
		${sender_address_domain}+$sender_address_local_part\
		@${sg {$primary_hostname}{^([^.]*)\..*}{\$1}}\
#		-${sg {$tod_log}{^.* ([0-9]+):([0-9]+):([0-9+])}{\$1\$2\$3}}\
		.SRS_DOMAIN
  errors_to = ${quote_local_part:BATV+${length_SRS_HASH_LENGTH:${hmac{md5}{SRS_SECRET}{${lc:$address_data}}}}+\
		${sg{$address_data}{(^.*)@[^@]*}{\$1}}}@\
		${sg{$address_data}{^.*@([^@]*)}{\$1}}
  driver = redirect
  data = ${quote_local_part:$local_part}@$domain
# Straight to output; don't start routing again from the beginning.
  redirect_router = lookuphost
  no_verify
.endif // SRS_DOMAIN

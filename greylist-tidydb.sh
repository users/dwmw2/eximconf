#!/bin/sh

# $Id: greylist-tidydb.sh,v 1.3 2005/12/18 21:19:21 dwmw2 Exp $

DBDIR=/var/spool/exim/db
exec > $DBDIR/greylist-db.$$
chown exim.exim $DBDIR/greylist-db.$$ &>/dev/null

NOW=`date +%s`
EXPIRE=`expr $NOW - 604800`

cat $DBDIR/greylist-db | (
    while read A ; do
	TIME=`echo $A | cut -f2 -d:`
	if [ $TIME -lt $EXPIRE ]; then
	    continue;
	else
	    echo $A
	fi
    done
    cat
)

mv $DBDIR/greylist-db.$$ $DBDIR/greylist-db	    
